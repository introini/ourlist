$(function() {
	$(':checkbox').click(function() {
		var list_id = $('.checkbox input:checked').val();
		$('#rename-form').attr('action', '/mylist/rename='+list_id);
	});
	$('#renameBtn').click(function() {
		$('.modal').addClass('is-active');
	});
	
	$('.delete').click(function() {
		$('.modal').removeClass('is-active');
	});
	
	$('a:contains(Cancel)').click(function() {
		$('.modal').removeClass('is-active');
	});
	
	$('#submit').click(function() {
		$('#rename-form').submit();
	});
});