from django import forms
from mylist.models import MovieList


class SearchForm(forms.Form):
    search = forms.CharField(widget=forms.TextInput(attrs={'type': 'text','class':'input',
                                                           'placeholder': 'The Martian'}),
                             label='Search', max_length=120)


class AddMovieForm(forms.Form):
    movie_lists = forms.ModelChoiceField(queryset=MovieList.objects.all(),empty_label='Select a list')

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(AddMovieForm, self).__init__(*args, **kwargs)

        if user is not None:
            self.fields['movie_lists'].queryset = MovieList.objects.filter(owner=user)
        else:
            self.fields['movie_lists'] = ['Not Logged In']