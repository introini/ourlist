from django.conf.urls import url
from . import views

app_name = 'search'

urlpatterns = [
    url(r'^$', views.TopThisMonth.as_view(), name='index'),
    url(r'^browse=', views.SearchView.as_view(), name='browse'),
    url(r'^movies$', views.BrowseMovies.as_view(), name='movies'),
    url(r'^genre=(?P<genre_id>[0-9]+)$', views.GenreView.as_view(), name='by_genre'),
    url(r'^year=(?P<year>[0-9]+)$', views.YearView.as_view(), name='by_year'),
    url(r'^top-this-month', views.TopThisMonth.as_view(), name='top-this-month'),
    url(r'^top-this-year', views.TopThisYear.as_view(), name='top-this-year'),
    url(r'^(?P<movie_id>[0-9]+)$', views.MovieDetail.as_view(), name='movie-detail'),
]
