from django.shortcuts import get_object_or_404, render
from .forms import SearchForm, AddMovieForm
from django.views.generic.base import TemplateView
from django.utils import timezone
from mylist.models import MovieListMembership
import tmdbsimple as tmdb
import yaml
import os


''' 
CONSTANTS:
- MAX_RESULTS: Sets the number of search results to return 
- CUR_YEAR: Sets the current year
- CONFIG: Config file to load
'''
MAX_RESULTS = 12
CUR_YEAR = timezone.now().year
CONFIG = 'configs.yml'

# Load TMDB API Key

''' Loads the config file that contains the API key for TheMovieDatabase.org '''
with open(os.path.join(os.path.dirname(os.path.abspath(__file__)),CONFIG), 'r') as ymlfile:
    cfg = yaml.load(ymlfile)
    tmdb.API_KEY = cfg['tmdb']['api_key']


# Retrieve poster url base from TMDB API
def get_poster_base():
    conf = tmdb.Configuration()
    secure_base_url = conf.info()['images']['secure_base_url']
    size = conf.info()['images']['poster_sizes'][2]
    url = [secure_base_url, size]
    return ''.join(url)


# Retrieve backdrop url from TMDB API
def get_backdrop_base():
    conf = tmdb.Configuration()
    secure_base_url = conf.info()['images']['secure_base_url']
    size = conf.info()['images']['backdrop_sizes'][2]
    url = [secure_base_url, size]
    return ''.join(url)


# Builds the list of years from 1900 to current year
def get_years(seed_year=1900):
    year_range = []
    for year in range(CUR_YEAR, seed_year, -1):
        year_range.append(year)
    return year_range


# Return a list of movie genres from TMDB API
def get_movie_genres():
    return tmdb.Genres().list()['genres']


# Main view for the Browse portion of the app
class IndexView(TemplateView):
    template_name = 'search/index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            watched_movies = MovieListMembership.objects.filter(owner=self.request.user.userprofile, watched=True)
        else:
            watched_movies = None
        form = SearchForm()
        context['form'] = form
        context['poster'] = get_poster_base()
        context['year_list'] = get_years()
        context['genre_list'] = get_movie_genres()

        return context


class BrowseMovies(IndexView):

    def get_context_data(self, **kwargs):
        context = super(BrowseMovies, self).get_context_data(**kwargs)
        d = tmdb.Discover()
        response = d.movie(sort_by='popularity.desc')
        context['browse_header'] = 'All Movies'
        context['response'] = response['results'][:MAX_RESULTS]
        return context


class SearchView(IndexView):

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        if request.method == 'GET':
            form = SearchForm(request.GET)
            if form.is_valid():
                q = tmdb.Search()
                response = q.movie(query=form.cleaned_data['search'])
                context['browse_header'] = 'Search Results'
                context['response'] = response['results'][:MAX_RESULTS]
                return render(request, 'search/index.html', context)
        else:
            form = SearchForm()

        return render(request, 'search/index.html', context)


class TopThisMonth(IndexView):

    def get_context_data(self, **kwargs):
        context = super(TopThisMonth, self).get_context_data(**kwargs)
        d = tmdb.Discover()
        t = timezone.now()
        last_month = t.replace(day=1) - timezone.timedelta(days=1)
        date = timezone.datetime(
            year=timezone.now().year,
            month=last_month.month,
            day=1
        )
        response = d.movie(primary_release_date_gte=date.date())
        context['browse_header'] = 'Top Movies since '
        context['date'] = date.date()
        context['response'] = response['results'][:MAX_RESULTS]

        return context


class TopThisYear(IndexView):

    def get_context_data(self, **kwargs):
        context = super(TopThisYear, self).get_context_data(**kwargs)
        d = tmdb.Discover()
        date = timezone.datetime(
            year=timezone.now().year,
            month=1,
            day=1
        )
        response = d.movie(primary_release_date_gte=date.date())
        context['browse_header'] = 'Top Movies since '
        context['date'] = date.date()
        context['response'] = response['results'][:MAX_RESULTS]

        return context


class GenreView(IndexView):

    def get_context_data(self, **kwargs):
        context = super(GenreView, self).get_context_data(**kwargs)
        d = tmdb.Discover()
        response = d.movie(with_genres=kwargs['genre_id'])
        genre_list = get_movie_genres()

        for item in genre_list:
            if str(item['id']) == kwargs['genre_id']:
                context['browse_header'] = item['name']

        context['response'] = response['results'][:MAX_RESULTS]

        return context


class YearView(IndexView):

    def get_context_data(self, **kwargs):
        context = super(YearView, self).get_context_data(**kwargs)
        d = tmdb.Discover()
        response = d.movie(year=kwargs['year'])
        context['browse_header'] = kwargs['year']
        context['response'] = response['results'][:MAX_RESULTS]
        return context


class MovieDetail(IndexView):

    template_name = 'search/movie-detail.html'

    def get_context_data(self, **kwargs):
        q = tmdb.Movies(kwargs['movie_id'])
        response = q.info()
        if self.request.user.is_authenticated:
            form = AddMovieForm(user=self.request.user.userprofile)
        else:
            form = AddMovieForm()

        context = {'movie': response, 'poster': get_poster_base(), 'backdrop': get_backdrop_base(), 'add_movie_form': form}
        return context
