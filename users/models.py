from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User
from django.db.models.signals import post_save


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    first_name = models.CharField(max_length=120, default='')
    last_name = models.CharField(max_length=120, default='')
    email = models.EmailField(max_length=256)
    friends = models.ManyToManyField('self', blank=True)

    def __str__(self):
        return "{0}'s Profile".format(self.user)

    def get_absolute_url(self):
        return reverse('user-settings-pi', kwargs={'pk': self.pk})


def create_profile(sender, **kwargs):
    if kwargs['created']:
        user_profile = UserProfile.objects.create(user=kwargs['instance'])

post_save.connect(create_profile, sender=User)
