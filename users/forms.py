from django.contrib.auth.models import User
from django.forms import ModelForm
from django.contrib.auth.forms import AuthenticationForm,  UserCreationForm
from django import forms
from .models import UserProfile


class UserLoginForm(AuthenticationForm): 
    username = forms.CharField(widget=forms.TextInput(
        attrs={'type': 'text', 'class': 'input',  'placeholder':  'Username'}))
    password = forms.CharField(widget=forms.PasswordInput(
        attrs={'type': 'password',  'class':  'input', 
               'placeholder':  'Password'}))

    class Meta(AuthenticationForm): 
        model = User
        fields = ['username',  'password']


class UserRegistrationForm(UserCreationForm): 
    username = forms.CharField(widget=forms.TextInput(
        attrs={'type': 'text', 'class': 'input',  'placeholder': 'Username'}))
    password1 = forms.CharField(widget=forms.PasswordInput(
        attrs={'type': 'password', 'class': 'input', 
               'placeholder': 'Password'}))
    password2 = forms.CharField(widget=forms.PasswordInput(
        attrs={'type': 'password', 'class': 'input', 
               'placeholder': 'Re-Enter Password'}))

    class Meta: 
        model = User
        fields = ['username',  'password1',  'password2']

    def save(self,  commit=True): 
        user = super(UserRegistrationForm,  self).save(commit=False)

        if commit: 
            user.save()
        return user


class UserAccountInfoForm(ModelForm):
    class Meta:
        model = User
        fields = ['username']


class UserPersonalInfoForm(ModelForm):
    first_name = forms.CharField(widget=forms.TextInput(
        attrs={'type': 'text', 'class': 'input', 'placeholder': 'First Name'}))
    last_name = forms.CharField(widget=forms.TextInput(
        attrs={'type': 'text', 'class': 'input', 'placeholder': 'Last Name'}))
    email = forms.EmailField(widget=forms.EmailInput(
        attrs={'type': 'email', 'class': 'input', 'placeholder': 'email@domaim.com'}))
    class Meta:
        model = UserProfile
        fields = ['first_name', 'last_name', 'email']



class SearchUsersForm(forms.Form):
    search = forms.CharField(widget=forms.TextInput(attrs={'type': 'text','class':'input',
                                                           'placeholder': 'Add a friend...'}),
                            max_length=120)
