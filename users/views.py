from django.contrib.auth.views import LoginView, LogoutView
from django.views.generic.edit import UpdateView
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from .models import UserProfile
from .forms import UserLoginForm, UserRegistrationForm, UserPersonalInfoForm, SearchUsersForm
from django.shortcuts import render, redirect

'''
Login Class

Class Based View to log a user in
'''
class Login(LoginView):
    title = 'Login'
    template_name = 'users/form.html'
    authentication_form = UserLoginForm
    extra_context = {'title': title}

'''
Logout Class

Class Based View to log a user out
'''
class Logout(LogoutView):
    title = 'Login'
    template_name = 'users/form.html'
    next_page = 'login'
    extra_context = {'title': title}


class UserSettings(UpdateView):
    model = UserProfile
    template_name = 'users/user_settings_form.html'
    form_class = UserPersonalInfoForm


def user_settings_fl(request,pk):
    qs = UserProfile.objects.get(pk=pk).friends.all()
    add_friend_form = SearchUsersForm()
    context = {'friends_list': qs, 'add_friend_form': add_friend_form}
    return render(request, 'users/user_friends_form.html', context)


def search_users(request,pk):
    fl = UserProfile.objects.get(pk=pk).friends.all()
    
    if request.method =='GET':
        add_friend_form = SearchUsersForm(request.GET or None)
        if add_friend_form.is_valid():
            query = add_friend_form.cleaned_data['search']
            qs = User.objects.filter(username__istartswith=query)
            context = {'users_found': qs, 'add_friend_form': add_friend_form, 'friends_list': fl}
        return render(request, 'users/user_friends_form.html', context)
    else:
        add_friend_form = SearchUsersForm()
        context = {'users_found': 'None', 'add_friend_form': add_friend_form, 'friends_list': fl}
    return render(request, 'users/user_friends_form.html', context) 


def add_friend(request,pk,add_pk):
    owner = UserProfile.objects.get(pk=pk)
    friend = UserProfile.objects.get(pk=add_pk)
    owner.friends.add(friend)
    qs = UserProfile.objects.get(pk=pk).friends.all()
    add_friend_form = SearchUsersForm()
    context = {'friends_list': qs, 'add_friend_form': add_friend_form}

    return render(request, 'users/user_friends_form.html', context)

def remove_friend(request,pk,remove_pk):
    owner = UserProfile.objects.get(pk=pk)
    friend = UserProfile.objects.get(pk=remove_pk)
    owner.friends.remove(friend)
    qs = UserProfile.objects.get(pk=pk).friends.all()
    add_friend_form = SearchUsersForm()
    context = {'friends_list': qs, 'add_friend_form': add_friend_form}

    return render(request, 'users/user_friends_form.html', context)

def register_view(request):
    # Returns a new 'register view', allowing new users to register a new account

    title = 'Register'
    # Instantiate a new form if we receive a POST request.
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST or None)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
        return redirect('/')
    else:
        form = UserRegistrationForm()
        context = {'form': form, 'title': title}
    return render(request, 'users/form.html', context)
