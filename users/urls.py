from django.conf.urls import include, url
from . import views

app_name = 'users'

urlpatterns = [
    url(r'^login/', views.Login.as_view(), name='login'),
    url(r'^register/', views.register_view, name='register'),
    url(r'^logout/', views.Logout.as_view(), name='logout'),
    url(r'^settings/(?P<pk>[0-9]+)/$', views.UserSettings.as_view(), name='user-settings-ai'),
    url(r'^settings/(?P<pk>[0-9]+)/$', views.UserSettings.as_view(), name='user-settings-pi'),
    url(r'^settings/fl=(?P<pk>[0-9]+)$', views.user_settings_fl, name='user-settings-fl'),
    url(r'^settings/fl=(?P<pk>[0-9]+)&search=$', views.search_users, name='search-users'),
    url(r'^settings/fl=(?P<pk>[0-9]+)&add=(?P<add_pk>[0-9]+)$', views.add_friend, name='add-friend'),
    url(r'^settings/fl=(?P<pk>[0-9]+)&remove=(?P<remove_pk>[0-9]+)$', views.remove_friend, name='remove-friend'),
]