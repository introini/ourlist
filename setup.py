# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='ourlist',
    version='0.1.0',
    description='Ourlist app',
    long_description=readme,
    author='Michael Introini',
    author_email='me@mintroni.com',
    url='https://github.com/mintadm/ourlist',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)

