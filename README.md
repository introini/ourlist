# OurList

## This project is currently on hold.

### Work in Progress

**OurList** is a simple need-based project, created to help ease the everlasting problem of deciding which movie my girlfriend and I would want to watch. It centers around the [Django](https://www.djangoproject.com/) framework for the backend, and uses the [Bulma](http://bulma.io/) css framework for the frontend.
