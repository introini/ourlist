from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import DetailView, TemplateView
from django.views.generic.edit import DeleteView, UpdateView
from .models import Movie, MovieList, MovieListMembership
from search.forms import SearchForm, AddMovieForm
from .forms import EditListForm, RenameListForm
import tmdbsimple as tmdb


'''
Simple percentage function for movie stats
'''


def get_percent(watched, total):
    if total > 0:
        return watched / total * 100
    else:
        return 0

'''
Main view, all other views in My Lists should inherit
from this one.
'''


class IndexView(LoginRequiredMixin, TemplateView):
    login_url = '/login/'
    template_name = 'mylist/index.html'

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(IndexView, self).get_context_data(**kwargs)
        owner = self.request.user.userprofile
        lists = MovieList.objects.filter(owner=owner)
        shared_with_me = MovieList.objects.filter(shared_with=owner)

        # Define Context
        extra_context = {
            'movie_count': MovieListMembership.objects.filter(owner=owner).count(),
            'my_lists': lists,
            'lists_shared_with_me': shared_with_me,
            'form': SearchForm(),
            'rename_form': RenameListForm(),
            'sidebar': 'mylist/sidebar_nav.html'
        }

        context.update(extra_context)

        return context


'''

Movie List Views

'''


class MovieListView(IndexView):
    model = MovieList
    template_name = 'mylist/movielist-detail.html'

    def get_context_data(self, **kwargs):
        context = super(MovieListView, self).get_context_data(**kwargs)
        list_id = self.kwargs['pk']
        owner = self.request.user.userprofile
        movies_in_list = MovieListMembership.objects.movies_for_list(list_id)
        list_watched_count = movies_in_list.filter(watched=True).count()
        all_movies = MovieListMembership.objects.filter(owner=owner)
        all_watched_count = all_movies.filter(watched=True).count()

        extra_context = {
            'movie_list': MovieListMembership.objects.movies_for_list(list_id),
            'list_id': list_id,
            'list': MovieList.objects.get(id=list_id),
            'movies_watched': list_watched_count,
            'watched_percent': get_percent(list_watched_count, movies_in_list.count()),
            'all_movies_watched': all_watched_count,
            'all_movies_watched_percent': get_percent(all_watched_count, all_movies.count()),
            'movies': movies_in_list,
            'rename_form': RenameListForm()
        }

        context.update(extra_context)

        return context


def rename_list(request, list_id, **kwargs):
    if request.method == 'POST':
        form = RenameListForm(request.POST)
        if form.is_valid():
            movie_list = get_object_or_404(MovieList, pk=list_id)
            movie_list.list_name = form.cleaned_data['list_name']
            movie_list.save()
        return HttpResponseRedirect(reverse('mylist:index', ))
    else:
        return render(request, 'mylist/index.html')


class MovieListEditView(UpdateView):
    model = MovieList
    form_class = EditListForm

    def get_context_data(self, **kwargs):
        owner = self.request.user.userprofile
        context = super(MovieListEditView, self).get_context_data(**kwargs)
        lists = MovieList.objects.filter(owner=owner)
        list_name = lists.filter(id=self.kwargs['pk'])

        extra_context = {
            'my_lists': lists,
            'list_id': self.kwargs['pk'],
            'list_name': self.get_form_kwargs()['instance'],
            'sidebar': 'mylist/sidebar_nav.html'
        }

        context.update(extra_context)

        return context

    def get_form_kwargs(self):
        kwargs = super(MovieListEditView, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

'''

Movie Detail Views

'''

class MovieDetailView(DetailView):
    model = Movie
    template_name = 'mylist/movie-detail.html'
    context_object_name = 'movie'


class MovieUpdateView(UpdateView):
    model = Movie
    fields = ['title', 'year', 'overview']
    template_name_suffix = '-update-form'


class MovieDeleteView(DeleteView):
    model = Movie
    context_object_name = 'item'
    success_url = reverse_lazy('mylist:index')


def add_new_list(request):
    if request.method == 'GET':
        new_list = MovieList.objects.create(owner=request.user.userprofile,
                                            list_name="New List " + str(timezone.now().microsecond))
        new_list.save()

    return HttpResponseRedirect(reverse('mylist:index', ))


def add_movie_to_list(request, movie_id):
    if request.method == 'POST':
        form = AddMovieForm(request.POST)
        if form.is_valid():
            m = tmdb.Movies(id=movie_id)
            movie_info = m.info()
            user_movie_list = get_object_or_404(MovieList, list_name=form.cleaned_data['movie_lists'])
            obj, movie = Movie.objects.get_or_create(
                tmdb_id=movie_id,
                title=movie_info['title'],
                release_date=movie_info['release_date'],
                overview=movie_info['overview'],
                runtime=movie_info['runtime'],
                backdrop_url=movie_info['backdrop_path'],
                poster_url=movie_info['poster_path']
            )
            movie_membership = MovieListMembership(movie=obj, movie_list=user_movie_list,
                                                   date_added=timezone.now(), owner=request.user.userprofile)
            movie_membership.save()
            return HttpResponseRedirect(reverse('search:movie-detail', args=(movie_id,)))
    else:
        return render(request, 'search/index.html')


def set_movie_watched(request, list_id, movie_id):
    MovieListMembership.objects.filter(movie=movie_id).update(watched=True, date_watched=timezone.now())
    return HttpResponseRedirect(reverse('mylist:list-detail', args=(list_id,)))


def clear_movie_watched(request, list_id, movie_id):
    MovieListMembership.objects.filter(movie=movie_id).update(watched=False, date_watched=None)
    return HttpResponseRedirect(reverse('mylist:list-detail', args=(list_id,)))
