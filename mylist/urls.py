from django.conf.urls import include, url
from . import views

app_name = 'mylist'

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^new_list$', views.add_new_list, name='new_list'),
    url(r'^rename=(?:(?P<list_id>[0-9]+))?$', views.rename_list, name='rename_list'),
    url(r'^list/(?P<pk>[0-9]+)/$', views.MovieListEditView.as_view(), name='list-edit'),
    url(r'^add=(?P<movie_id>[0-9]+)', views.add_movie_to_list, name='add_to_list'),
    url(r'^(?P<pk>[0-9]+)/list/$', views.MovieListView.as_view(), name='list-detail'),
    url(r'^(?P<pk>[0-9]+)/detail/$', views.MovieDetailView.as_view(), name='movie-detail'),
    url(r'^(?P<pk>[0-9]+)/edit/$', views.MovieUpdateView.as_view(), name='movie-update'),
    url(r'^(?P<pk>[0-9]+)/delete/$', views.MovieDeleteView.as_view(), name='movie-delete'),
    url(r'^list=(?P<list_id>[0-9]+)&set_watched=(?P<movie_id>[0-9]+)', views.set_movie_watched, name='set_watched'),
    url(r'^list=(?P<list_id>[0-9]+)&clear_watched=(?P<movie_id>[0-9]+)', views.clear_movie_watched, name='clear_watched'),


]
