from django.contrib import admin
from .models import Movie, MovieList, MovieListMembership
#from .models import Genre
# Register your models here.


class MovieListModelAdmin(admin.ModelAdmin):
    list_display = ['owner','list_name', 'date_created']
    fields = ('owner','list_name', 'shared', 'shared_with')

    class Meta:
        model = MovieList


class MovieListMembershipModelAdmin(admin.ModelAdmin):
    list_display = ['movie', 'movie_list', 'owner']

    class Meta:
        model = MovieListMembership

admin.site.register(Movie)
#admin.site.register(Genre)
admin.site.register(MovieList, MovieListModelAdmin)
admin.site.register(MovieListMembership, MovieListMembershipModelAdmin)