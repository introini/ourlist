from django.urls import reverse
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from users.models import UserProfile
import tmdbsimple as tmdb
import os
import yaml

with open(os.path.join(os.path.dirname(os.path.abspath(__file__)),"configs.yml"), 'r') as ymlfile:
    cfg = yaml.load(ymlfile)
    tmdb.API_KEY = cfg['tmdb']['api_key']


class Movie(models.Model):
    tmdb_id = models.IntegerField(blank=False, null=True)
    title = models.CharField(max_length=120,unique=True)
    release_date = models.CharField(max_length=32, null=True, blank=True)
    overview = models.TextField()
    runtime = models.CharField(max_length=12, blank=True, null=True)
    backdrop_url = models.CharField(max_length=512, null=True, blank=True)
    poster_url = models.CharField(max_length=512,null=True, blank=True)
    last_edited = models.DateTimeField(auto_now=True)

    def get_absolute_url(self):
        return reverse('mylist:index')

    def get_image(self):
        conf = tmdb.Configuration()
        base_url = conf.info()['images']['base_url']
        size = conf.info()['images']['poster_sizes'][2]
        m = tmdb.Movies(self.id)
        url_parts = [base_url,size,m.info()['poster_path']]

        return ''.join(url_parts)

    def __str__(self):
        return self.title

'''
class Genre(models.Model):
    genre_id = models.IntegerField(null=True, blank=False)
    name = models.CharField(max_length=64)
    movie = models.ManyToManyField(Movie)
'''


class MovieList(models.Model):
    owner = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    list_name = models.CharField(max_length=120, unique=True)
    date_created = models.DateTimeField(auto_now=False, auto_now_add=True)
    shared = models.BooleanField(default=False)
    shared_with = models.ManyToManyField(UserProfile, blank=True, related_name='shared_with')
    movies = models.ManyToManyField(Movie, through='MovieListMembership', through_fields=('movie_list','movie'))

    def get_absolute_url(self):
        return reverse('mylist:index')

    def __str__(self):
        return self.list_name


class MovieListMembershipManager(models.Manager):
    def movies_for_list(self, list):
        return self.get_queryset().filter(movie_list=list)


class MovieListMembership(models.Model):
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    movie_list = models.ForeignKey(MovieList, on_delete=models.CASCADE)
    rating = models.CharField(max_length=12, null=True, blank=True)
    favorite = models.BooleanField(default=False)
    watched = models.BooleanField(default=False)
    date_added = models.DateTimeField(auto_now=False, auto_now_add=True)
    date_watched = models.DateTimeField(null=True, blank=True)
    owner = models.ForeignKey(UserProfile, on_delete=models.CASCADE, related_name='movie_list_owner')

    objects = MovieListMembershipManager()

    def get_movie_title(self):
        return self.movie

    def get_movie_overview(self):
        return self.movie.overview

    def get_list_name(self):
        return self.move_list

    def __str__(self):
        return self.owner.user.username