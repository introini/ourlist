from django import forms
from django.forms import ModelForm, TextInput, CheckboxInput
from mylist.models import MovieList
from users.models import UserProfile


class RenameListForm(forms.Form):
    list_name = forms.CharField(widget=TextInput(attrs={'type': 'text','class':'input',
                                                        'placeholder': 'List Name'}),
                                label='List Name', max_length=120)


class EditListForm(ModelForm):

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super(EditListForm, self).__init__(*args, **kwargs)
        self.fields['shared_with'].queryset = UserProfile.objects.exclude(user=user)

    class Meta:
        model = MovieList
        fields = ['list_name', 'shared', 'shared_with']
        widgets = {
            'list_name': TextInput(attrs={'type': 'text','class':'input',
                                          'placeholder': 'List Name'}),
            'shared': CheckboxInput(attrs={'type': 'checkbox'}),
        }
